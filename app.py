from flask import Flask, render_template
import os

app = Flask(__name__)


@app.route("/")
def wish():
    name=os.getenv("NAME", "Andres") 
    return render_template('index.html', name=name)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
