terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider33
provider "aws" {
  region = "us-east-1"
  profile = "dev"
}

resource "aws_security_group" "ssh-sg" {
  name = "ssh-sg"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  
}

resource "aws_instance" "instance-prod" {
  ami           = "ami-087c17d1fe0178315"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.ssh-sg.id]
}
